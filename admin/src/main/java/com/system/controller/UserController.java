package com.system.controller;

import cn.hserver.plugin.web.annotation.apidoc.ApiImplicitParam;
import cn.hserver.plugin.web.annotation.apidoc.ApiImplicitParams;
import cn.hserver.plugin.web.annotation.apidoc.DataType;
import cn.hserver.plugin.web.interfaces.HttpRequest;
import com.system.jwt.Token;
import com.system.jwt.TokenUtil;
import com.system.domain.dto.LoginDTO;
import com.system.domain.vo.LoginVO;
import com.system.domain.dto.UserDTO;
import com.system.domain.vo.UserInfoVO;
import com.system.service.TokenService;
import cn.hserver.plugin.web.annotation.*;

import com.system.service.UserService;
import cn.hserver.core.ioc.annotation.*;
import cn.hserver.core.server.util.JsonResult;

import java.util.List;

/**
 * @author hxm
 */
@Controller(value = "/system/user/", name = "用户管理")
public class UserController extends BaseController {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @POST("login")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(
                            name = "password", value = "密码", required = true, dataType = DataType.String
                    ), @ApiImplicitParam(
                    name = "username", value = "用户名", required = true, dataType = DataType.String
            )
            },
            name = "登录接口",
            note = "后端的登录接口"
    )
    public JsonResult login(LoginDTO loginDTO) {
        LoginVO login = userService.login(loginDTO);
        return login == null ? JsonResult.error("登录失败") : JsonResult.ok().put("data", login);
    }

    @RequiresPermissions(value = "get:/system/user/info", name = "获取用户信息")
    @GET("info")
    public JsonResult info(String token) {
        Token tokenInfo = TokenUtil.getToken(token);
        UserInfoVO userInfo = userService.getUserInfo(tokenInfo.getUserId());
        return JsonResult.ok().put("data", userInfo);
    }

    @RequiresPermissions(value = "post:/system/user/logout", name = "登出")
    @POST("logout")
    public JsonResult logout(HttpRequest request) {
        Integer userId = getUserId(request);
        tokenService.removeToken(userId);
        return JsonResult.ok();
    }

    @RequiresPermissions(value = "get:/system/user/list", name = "用户列表")
    @GET("list")
    public JsonResult list(Integer page, Integer pageSize) {
        return JsonResult.ok().put("data", userService.list(page, pageSize));
    }

    @RequiresPermissions(value = "post:/system/user/edit", name = "用户列表")
    @POST("edit")
    public JsonResult edit(UserDTO userDTO) {
        userService.edit(userDTO);
        return JsonResult.ok();
    }

    @RequiresPermissions(value = "post:/system/user/add", name = "添加用户")
    @POST("add")
    public JsonResult add(UserDTO userDTO) {
        userService.add(userDTO);
        return JsonResult.ok();
    }


    @RequiresPermissions(value = "get:/system/user/delete", name = "删除用户")
    @GET("delete")
    public JsonResult delete(Integer id) {
        userService.delete(id);
        return JsonResult.ok();
    }

    @RequiresPermissions(value = "post:/system/user/multiDelete", name = "删除多个用户")
    @POST("multiDelete")
    public JsonResult multiDelete(List<Integer> ids) {
        userService.multiDelete(ids);
        return JsonResult.ok();
    }

}
