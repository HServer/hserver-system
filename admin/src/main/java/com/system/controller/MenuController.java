package com.system.controller;

import cn.hserver.plugin.web.annotation.Controller;
import cn.hserver.plugin.web.annotation.GET;
import cn.hserver.plugin.web.annotation.POST;
import cn.hserver.plugin.web.annotation.RequiresPermissions;
import cn.hserver.plugin.web.interfaces.HttpRequest;
import com.system.domain.entity.MenuEntity;
import com.system.service.MenuService;
import cn.hserver.core.ioc.annotation.*;
import cn.hserver.core.server.util.JsonResult;

/**
 * @author hxm
 */
@Controller(value = "/system/menu/", name = "菜单管理")
public class MenuController extends BaseController {

    @Autowired
    protected MenuService menuService;


    @RequiresPermissions(value = "get:/system/menu/list",name = "获取全部菜单列表")
    @GET("list")
    public JsonResult list() {
        return JsonResult.ok().put("data", menuService.list());
    }


    @RequiresPermissions(value = "get:/system/menu/menu",name = "获取当前用户的列表")
    @GET("menu")
    public JsonResult menu(HttpRequest request) {
        return JsonResult.ok().put("data", menuService.menu(getRoleIds(request)));
    }



    @RequiresPermissions(value = "get:/system/menu/listTop",name = "获取一二级的菜单")
    @GET("listTop")
    public JsonResult listTop() {
        return JsonResult.ok().put("data", menuService.listTop());
    }

    @RequiresPermissions(value = "post:/system/menu/updateMenu",name = "更新菜单")
    @POST("updateMenu")
    public JsonResult updateMenu(MenuEntity menuEntity) {
        if (menuEntity.getId() != null) {
            menuService.updateMenu(menuEntity);
            return JsonResult.ok();
        }
        return JsonResult.error();
    }

    @RequiresPermissions(value = "post:/system/menu/addMenu",name = "添加菜单")
    @POST("addMenu")
    public JsonResult addMenu(MenuEntity menuEntity) {
        menuService.addMenu(menuEntity);
        return JsonResult.ok();
    }

    @RequiresPermissions(value = "get:/system/menu/deleteMenu",name = "删除菜单")
    @GET("deleteMenu")
    public JsonResult deleteMenu(Integer id) {
        menuService.deleteMenu(id);
        return JsonResult.ok();
    }

}
