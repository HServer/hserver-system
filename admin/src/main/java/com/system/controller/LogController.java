package com.system.controller;

import cn.hserver.plugin.web.annotation.Controller;
import cn.hserver.plugin.web.annotation.GET;
import cn.hserver.plugin.web.annotation.POST;
import cn.hserver.plugin.web.annotation.RequiresPermissions;
import com.system.domain.entity.LogEntity;
import com.system.service.LogService;
import cn.hserver.core.ioc.annotation.*;
import cn.hserver.core.server.util.JsonResult;

import java.util.Map;

@Controller(value = "/system/log", name = "日志管理")
public class LogController {

    @Autowired
    private LogService logService;

    @RequiresPermissions(value = "/system/log/getLogList", name = "获取日志表")
    @GET("/getLogList")
    public JsonResult getLogList(Map<String, Object> map) {
        return JsonResult.ok().put("data", logService.getLogList(map));
    }

    @RequiresPermissions(value = "/system/log/delLog", name = "删除日志表")
    @GET("/delLog")
    public JsonResult delLog(Integer id) {
        return logService.delLog(id) ? JsonResult.ok() : JsonResult.error();
    }

    @RequiresPermissions(value = "/system/log/addLog", name = "新增日志表")
    @POST("/addLog")
    public JsonResult addLog(LogEntity logEntity) {
        return logService.addLog(logEntity) ? JsonResult.ok() : JsonResult.error();
    }

    @RequiresPermissions(value = "/system/log/updLog", name = "修改日志表")
    @POST("/updLog")
    public JsonResult updLog(LogEntity logEntity) {
        return logService.updLog(logEntity) ? JsonResult.ok() : JsonResult.error();
    }
}
