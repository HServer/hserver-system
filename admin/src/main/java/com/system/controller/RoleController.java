package com.system.controller;

import cn.hserver.plugin.web.annotation.*;
import com.system.domain.entity.RoleEntity;
import com.system.domain.dto.MpDTO;
import com.system.service.RoleService;
import cn.hserver.core.ioc.annotation.*;
import cn.hserver.core.server.util.JsonResult;

/**
 * @author hxm
 */
@Controller(value = "/system/role/", name = "角色管理")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequiresPermissions(value = "get:/system/role/list",name = "角色列表")
    @GET("list")
    public JsonResult list() {
        return JsonResult.ok().put("data", roleService.list());
    }

    @RequiresPermissions(value = "post:/system/role/add",name = "添加角色")
    @POST("add")
    public JsonResult add(RoleEntity roleEntity) {
        roleService.add(roleEntity);
        return JsonResult.ok();
    }

    @RequiresPermissions(value = "get:/system/role/deleteRole",name = "删除角色")
    @GET("deleteRole")
    public JsonResult deleteRole(Integer id) {
        roleService.deleteRole(id);
        return JsonResult.ok();
    }

    @RequiresPermissions(value = "post:/system/role/updateRole",name = "更新角色")
    @POST("updateRole")
    public JsonResult updateRole(RoleEntity roleEntity) {
        if (roleEntity.getId() != null) {
            roleService.updateRole(roleEntity);
            return JsonResult.ok();
        }
        return JsonResult.error();
    }

    @RequiresPermissions(value = "post:/system/role/updateMenuPermission",name = "更新菜单权限")
    @POST("updateMenuPermission")
    public JsonResult updateMenuPermission(MpDTO mpDTO) {
        if (mpDTO != null) {
            roleService.updateMenuPermission(mpDTO);
            return JsonResult.ok();
        }
        return JsonResult.error();
    }

    @RequiresPermissions(value = "post:/system/role/updateModulePermission",name = "更新模块权限")
    @POST("updateModulePermission")
    public JsonResult updateModulePermission(MpDTO mpDTO) {
        if (mpDTO != null) {
            roleService.updateModulePermission(mpDTO);
            return JsonResult.ok();
        }
        return JsonResult.error();
    }

    @RequiresPermissions(value = "get:/system/role/getMenuPermission",name = "获取菜单权限")
    @GET("getMenuPermission")
    public JsonResult getMenuPermission(Integer roleId) {
        if (roleId != null) {
            return JsonResult.ok().put("data", roleService.getMenuPermission(roleId));
        }
        return JsonResult.error();
    }

    @RequiresPermissions(value = "get:/system/role/getModulePermission",name = "获取模块权限")
    @GET("getModulePermission")
    public JsonResult getModulePermission(Integer roleId) {
        if (roleId != null) {
            return JsonResult.ok().put("data", roleService.getModulePermission(roleId));
        }
        return JsonResult.error();
    }
}
