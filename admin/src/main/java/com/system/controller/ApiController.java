package com.system.controller;

import cn.hserver.plugin.web.annotation.Controller;
import cn.hserver.plugin.web.annotation.GET;
import cn.hserver.plugin.web.api.ApiDoc;
import cn.hserver.plugin.web.api.ApiResult;
import cn.hserver.plugin.web.interfaces.HttpResponse;
import com.system.Application;
import cn.hserver.core.ioc.annotation.*;
import cn.hserver.core.server.util.JsonResult;

import java.util.HashMap;
import java.util.List;

/**
 * @author hxm
 */
@Controller(value = "/system/api/", name = "API管理")
public class ApiController {

    @GET("index")
    public void index(HttpResponse httpResponse) {
        ApiDoc apiDoc = new ApiDoc(Application.class);
        try {
            List<ApiResult> apiData = apiDoc.getApiData();
            HashMap<String, Object> stringObjectHashMap = new HashMap<>(2);
            stringObjectHashMap.put("data", apiData);
            httpResponse.sendTemplate("api.ftl", stringObjectHashMap);
        } catch (Exception e) {
            httpResponse.sendJson(JsonResult.error());
        }
    }


}
