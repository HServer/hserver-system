package com.system.controller;

import cn.hserver.plugin.web.annotation.Controller;
import cn.hserver.plugin.web.annotation.GET;
import cn.hserver.plugin.web.annotation.RequiresPermissions;
import com.system.service.ModuleService;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.server.util.JsonResult;


/**
 * @author hxm
 */
@Controller(value = "/system/module/", name = "模块管理")
public class ModuleController {

    @Autowired
    private ModuleService moduleService;

    @RequiresPermissions(value = "get:/system/module/sync",name = "同步权限")
    @GET("sync")
    public JsonResult sync() {
        moduleService.sync();
        return JsonResult.ok();
    }

    @GET("list")
    @RequiresPermissions(value = "get:/system/module/list",name = "所有权限列表")
    public JsonResult list() {
        return JsonResult.ok().put("data", moduleService.list());
    }

    @GET("getModuleList")
    @RequiresPermissions(value = "get:/system/module/getModuleList",name = "获取模块列表")
    public JsonResult getModuleList() {
        return JsonResult.ok().put("data", moduleService.getModuleList());
    }
}
