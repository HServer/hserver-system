package com.system.controller;

import cn.hserver.plugin.web.annotation.Controller;
import cn.hserver.plugin.web.annotation.GET;
import com.system.domain.entity.TableInfoEntity;
import com.system.gen.Gen;
import com.system.service.GenService;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.server.util.JsonResult;

import java.util.List;

@Controller(value = "/system/gen/", name = "代码生成工具")
public class GenController {

    @Autowired
    private GenService genService;

    @GET("list")
    public JsonResult list() {
        return JsonResult.ok().put("data", genService.genEntityList());
    }


    @GET("gen")
    public JsonResult gen(String table,String name) {
        List<TableInfoEntity> tableInfo = genService.getTableInfo(table);
        //生成实体
        Gen.genEntity(name,table,tableInfo);
        Gen.genMapper(name,table,tableInfo);
        Gen.genDao(name,table,tableInfo);
        Gen.genService(name,table,tableInfo);
        Gen.genController(name,table,tableInfo);
        Gen.genViewJs(name,table,tableInfo);
        Gen.genView(name,table,tableInfo);
        return JsonResult.ok();
    }


}
