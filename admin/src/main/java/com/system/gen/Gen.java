package com.system.gen;

import cn.hserver.core.server.context.ConstConfig;
import cn.hutool.core.io.file.FileWriter;
import com.system.domain.entity.TableInfoEntity;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class Gen {

    public static String genController() {
        try {
            HashMap<String, Object> data = new HashMap<>();
            return CodeTemplate.getTemplate("/controller.ftl", data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void genEntity(String name, String table, List<TableInfoEntity> tableInfo) {
        try {
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", name);
            data.put("tableName", table);
            data.put("tableList", tableInfo.stream().map(TableField::new).collect(Collectors.toList()));
            String template = CodeTemplate.getTemplate("/entity.ftl", data);
            FileWriter writer = new FileWriter(ConstConfig.PATH + "code" + File.separator + name + File.separator + name + "Entity.java");
            writer.write(template);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void genMapper(String name, String table, List<TableInfoEntity> tableInfo) {
        try {
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", name);
            data.put("tableName", table);
            data.put("tableList", tableInfo.stream().map(TableField::new).collect(Collectors.toList()));
            String template = CodeTemplate.getTemplate("/mapper.ftl", data);
            System.out.println(template);
            FileWriter writer = new FileWriter(ConstConfig.PATH + "code" + File.separator + name + File.separator + name + "EntityMapper.xml");
            writer.write(template);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void genDao(String name, String table, List<TableInfoEntity> tableInfo) {
        try {
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", name);
            data.put("nameLower", name.toLowerCase());
            data.put("tableName", table);
            data.put("tableList", tableInfo.stream().map(TableField::new).collect(Collectors.toList()));
            String template = CodeTemplate.getTemplate("/dao.ftl", data);
            System.out.println(template);
            FileWriter writer = new FileWriter(ConstConfig.PATH + "code" + File.separator + name + File.separator + name + "Dao.java");
            writer.write(template);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void genService(String name, String table, List<TableInfoEntity> tableInfo) {
        try {
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", name);
            data.put("nameLower", name.toLowerCase());
            data.put("tableName", table);
            data.put("tableList", tableInfo.stream().map(TableField::new).collect(Collectors.toList()));
            String template = CodeTemplate.getTemplate("/service.ftl", data);
            System.out.println(template);
            FileWriter writer = new FileWriter(ConstConfig.PATH + "code" + File.separator + name + File.separator + name + "Service.java");
            writer.write(template);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void genController(String name, String table, List<TableInfoEntity> tableInfo) {
        try {
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", name);
            data.put("nameLower", name.toLowerCase());
            data.put("tableName", table);
            data.put("tableList", tableInfo.stream().map(TableField::new).collect(Collectors.toList()));
            String template = CodeTemplate.getTemplate("/controller.ftl", data);
            System.out.println(template);
            FileWriter writer = new FileWriter(ConstConfig.PATH + "code" + File.separator + name + File.separator + name + "Controller.java");
            writer.write(template);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void genViewJs(String name, String table, List<TableInfoEntity> tableInfo) {
        try {
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", name);
            data.put("nameLower", name.toLowerCase());
            data.put("tableName", table);
            data.put("tableList", tableInfo.stream().map(TableField::new).collect(Collectors.toList()));
            String template = CodeTemplate.getTemplate("/viewjs.ftl", data);
            System.out.println(template);
            FileWriter writer = new FileWriter(ConstConfig.PATH + "code" + File.separator + name + File.separator +  name.toLowerCase() + ".js");
            writer.write(template);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void genView(String name, String table, List<TableInfoEntity> tableInfo) {
        try {
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", name);
            data.put("nameLower", name.toLowerCase());
            data.put("tableName", table);
            data.put("tableList", tableInfo.stream().map(TableField::new).collect(Collectors.toList()));
            String template = CodeTemplate.getTemplate("/view.ftl", data);
            System.out.println(template);
            FileWriter writer = new FileWriter(ConstConfig.PATH + "code" + File.separator + name + File.separator +  name.toLowerCase() + ".vue");
            writer.write(template);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
