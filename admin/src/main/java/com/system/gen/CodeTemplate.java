package com.system.gen;

import cn.hserver.plugin.web.util.FreemarkerUtil;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

import java.io.StringWriter;
import java.util.Map;

public class CodeTemplate {

    private static final Configuration CFG;


    public static String getTemplate(String template, Map map) throws Exception {
        Template temp = CFG.getTemplate(template);
        StringWriter stringWriter = new StringWriter();
        temp.process(map, stringWriter);
        return stringWriter.toString();
    }

    static {
        CFG = new Configuration(Configuration.VERSION_2_3_27);
        CFG.setClassForTemplateLoading(cn.hserver.plugin.web.util.FreemarkerUtil.class, "/code");
        CFG.setTemplateLoader(new ClassTemplateLoader(cn.hserver.plugin.web.util.FreemarkerUtil.class, "/code"));
        CFG.setDefaultEncoding("UTF-8");
        CFG.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        CFG.setLogTemplateExceptions(false);
        CFG.setWrapUncheckedExceptions(true);
    }


}
