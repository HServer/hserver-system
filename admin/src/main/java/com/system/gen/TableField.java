package com.system.gen;

import com.system.domain.entity.TableInfoEntity;
import lombok.Data;

@Data
public class TableField {

    private String comment;

    private String name;

    private String type;

    private String javaName;

    private String javaType;

    private boolean hasPri;

    public TableField() {
    }

    public TableField(TableInfoEntity tableInfoEntity) {
        this.type = tableInfoEntity.getType();
        this.name = tableInfoEntity.getField();
        this.comment = tableInfoEntity.getComment();
        this.hasPri = !"".equals(tableInfoEntity.getKey());
    }

    public String getJavaType() {
        if (type.contains("bigint")) {
            return "Long";
        } else if (type.contains("int")) {
            return "Integer";
        } else if (type.contains("varchar")) {
            return "String";
        } else if (type.contains("datetime")) {
            return "Date";
        } else if (type.contains("decimal")) {
            return "Double";
        } else if (type.contains("double")) {
            return "Double";
        } else if (type.contains("timestamp")) {
            return "Long";
        }
        return "String";
    }


    public String getJavaName() {
        String s = this.name;
        if (s == null) {
            return null;
        }
        s = s.toLowerCase();
        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '_') {
                upperCase = true;
            } else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
