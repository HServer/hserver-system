package com.system;

import cn.hserver.HServerApplication;
import cn.hserver.core.ioc.annotation.HServerBoot;

/**
 * @author hxm
 */
@HServerBoot
public class Application {
    public static void main(String[] args) {
        HServerApplication.run(Application.class, 8888, args);
    }
}
