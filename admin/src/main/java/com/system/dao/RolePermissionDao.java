package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.system.domain.entity.RolePermissionEntity;

/**
 * @author hxm
 */
@Mybatis
public interface RolePermissionDao extends BaseMapper<RolePermissionEntity> {


    default void deleteMenuPermission(Integer roleId) {
        delete(new LambdaQueryWrapper<RolePermissionEntity>().eq(RolePermissionEntity::getType,0).eq(RolePermissionEntity::getRoleId,roleId));
    }

    default void updateModulePermission(Integer roleId) {
        delete(new LambdaQueryWrapper<RolePermissionEntity>().eq(RolePermissionEntity::getType,1).eq(RolePermissionEntity::getRoleId,roleId));
    }

}
