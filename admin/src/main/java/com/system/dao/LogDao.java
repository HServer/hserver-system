package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.system.domain.entity.LogEntity;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

@Mybatis
public interface LogDao extends BaseMapper<LogEntity> {
    IPage<LogEntity> listPage(Page<?> page, @Param("data") Map<String, Object> data);
}
