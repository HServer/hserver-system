package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.system.domain.entity.UserEntity;
import com.system.domain.vo.UserVO;

import java.util.List;

/**
 * @author hxm
 */
@Mybatis
public interface UserDao extends BaseMapper<UserEntity> {


    default UserEntity findUserByUsername(String username) {
        List<UserEntity> select = selectList(new LambdaQueryWrapper<UserEntity>().eq(UserEntity::getUsername, username));
        if (select.isEmpty()) {
            return null;
        }
        return select.get(0);
    }

    default UserEntity findUserByUserId(Integer userId) {
        List<UserEntity> select = selectList(new LambdaQueryWrapper<UserEntity>().eq(UserEntity::getId, userId));
        if (select.isEmpty()) {
            return null;
        }
        return select.get(0);
    }

    List<UserVO.UserRoleVO> selectRole(Integer userId);


    IPage<UserVO> listPage(Page<?> page);

    default IPage<UserVO> list(long pageNumber, long pageSize) {
        IPage<UserVO> userVOIPage = listPage(new Page<>(pageNumber, pageSize));
        for (UserVO userVO : userVOIPage.getRecords()) {
            userVO.setRoles(selectRole(userVO.getId()));
        }
        return userVOIPage;
    }

}
