package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.system.domain.entity.UserRoleEntity;

/**
 * @author hxm
 */
@Mybatis
public interface UserRoleDao extends BaseMapper<UserRoleEntity> {

    default boolean deleteUserId(Integer userId) {
        return delete(new LambdaQueryWrapper<UserRoleEntity>().eq(UserRoleEntity::getUserId,userId))>0;
    }

}
