package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.system.domain.entity.TableEntity;
import com.system.domain.entity.TableInfoEntity;

import java.util.List;

/**
 * @author hxm
 */
@Mybatis
public interface TableDao  {

    List<TableEntity> getTables(String dataBaseName);

    List<TableInfoEntity> getTableInfo(String tableName);

}
