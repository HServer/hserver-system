package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.system.domain.entity.MenuEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author hxm
 */
@Mybatis
public interface MenuDao extends BaseMapper<MenuEntity> {
    /**
     * 按角色ID查找菜单
     *
     * @param roleIds
     * @return
     */
    List<MenuEntity> findMenuByRoleIds(@Param("roleIds") Integer[] roleIds);
}
