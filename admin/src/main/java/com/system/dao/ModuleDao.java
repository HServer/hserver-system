package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.system.domain.entity.ModuleEntity;

/**
 * @author hxm
 */
@Mybatis
public interface ModuleDao extends BaseMapper<ModuleEntity> {

    default boolean deleteAll() {
        return delete(new QueryWrapper<>()) > 0;
    }
}
