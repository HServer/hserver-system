package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.system.domain.entity.TokenEntity;

/**
 * @author hxm
 */
@Mybatis
public interface TokenDao extends BaseMapper<TokenEntity> {

    /**
     * 删除这个用户的所有ID
     *
     * @param userId
     * @return
     */
    default boolean removeToken(Integer userId) {
        return delete(new LambdaQueryWrapper<TokenEntity>().eq(TokenEntity::getUserId,userId))>0;
    }
}
