package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.system.domain.entity.RoleEntity;

/**
 * @author hxm
 */
@Mybatis
public interface RoleDao extends BaseMapper<RoleEntity> {

}
