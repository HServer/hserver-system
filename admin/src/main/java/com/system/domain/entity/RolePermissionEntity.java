package com.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author hxm
 */
@Data
@TableName("sys_role_permission")
public class RolePermissionEntity {

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * type=0 菜单 类型
     * type=1 module 类型
     */
    private Integer type;

    private Integer roleId;

    private Integer mId;

    private String permission;

}
