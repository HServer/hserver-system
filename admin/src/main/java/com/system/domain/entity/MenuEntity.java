package com.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author hxm
 */
@Data
@TableName("sys_menu")
public class MenuEntity {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String path;
    private String component;
    private String redirect;
    private String name;
    private String title;
    private String icon;
    private Integer parentId;
    private Integer sort;


}
