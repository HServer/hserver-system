package com.system.domain.entity;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * SELECT *,TABLE_COMMENT 描述 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'hserver_db'  ORDER BY TABLE_NAME ASC
 */
public class TableEntity {

    private String tableName;

    private String engine;

    private String tableComment;

    private String tableCollation;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public String getTableCollation() {
        return tableCollation;
    }

    public void setTableCollation(String tableCollation) {
        this.tableCollation = tableCollation;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "TableEntity{" +
                "tableName='" + tableName + '\'' +
                ", engine='" + engine + '\'' +
                ", tableComment='" + tableComment + '\'' +
                ", tableCollation='" + tableCollation + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
