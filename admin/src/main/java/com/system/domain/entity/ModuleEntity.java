package com.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author hxm
 */
@Data
@TableName("sys_module")
public class ModuleEntity {

    @TableId(type = IdType.INPUT)
    private String permission;

    private String controllerName;

    private String name;


}
