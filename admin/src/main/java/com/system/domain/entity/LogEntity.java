package com.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("sys_log")
public class LogEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer userId;
    private String method;
    private Date createTime;
}
