package com.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author hxm
 */
@Data
@TableName("sys_role")
public class RoleEntity {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    @TableField("`desc`")
    private String desc;
}
