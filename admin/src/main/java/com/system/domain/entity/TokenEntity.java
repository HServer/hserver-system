package com.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author hxm
 */
@Data
@TableName("sys_token")
public class TokenEntity {

    @TableId(type = IdType.INPUT)
    private String token;
    private Integer userId;
    private Date createTime;

}
