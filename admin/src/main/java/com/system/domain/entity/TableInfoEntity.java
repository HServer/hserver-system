package com.system.domain.entity;

/**
 * SHOW FULL COLUMNS FROM sys_menu
 */
public class TableInfoEntity {

    private String field;

    private String type;

    private String key;

    private String comment;

    private String collation;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCollation() {
        return collation;
    }

    public void setCollation(String collation) {
        this.collation = collation;
    }

    @Override
    public String toString() {
        return "TableInfoEntity{" +
                "field='" + field + '\'' +
                ", type='" + type + '\'' +
                ", key='" + key + '\'' +
                ", comment='" + comment + '\'' +
                ", collation='" + collation + '\'' +
                '}';
    }
}

