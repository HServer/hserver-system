package com.system.domain.dto;

import lombok.Data;

/**
 * @author hxm
 */
@Data
public class LoginDTO {

    private String password;

    private String username;
}
