package com.system.domain.dto;

import lombok.Data;

import java.util.List;

/**
 * @author hxm
 */
@Data
public class MpDTO {
    private List<String> ids;
    private Integer roleId;
}
