package com.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.system.dao.LogDao;
import com.system.domain.entity.LogEntity;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.ioc.annotation.Bean;

import java.util.Date;
import java.util.Map;

@Bean
public class LogService {

    @Autowired
    private LogDao logDao;

    public IPage<LogEntity> getLogList(Map<String, Object> map) {
       Page<LogEntity> pages = new Page<>(Integer.parseInt(map.get("page").toString()), Integer.parseInt(map.get("pageSize").toString()));
        return logDao.listPage(pages,map);
    }

    public boolean delLog(Integer id) {
        return logDao.deleteById(id) > 0;
    }

    public boolean addLog(LogEntity logEntity) {
        logEntity.setCreateTime(new Date());
        return logDao.insert(logEntity) > 0;
    }

    public boolean updLog(LogEntity logEntity) {
        return logDao.updateById(logEntity) > 0;
    }
}
