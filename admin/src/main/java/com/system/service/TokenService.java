package com.system.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.system.dao.TokenDao;
import com.system.domain.entity.TokenEntity;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.ioc.annotation.Bean;

import java.util.Date;
import java.util.List;

/**
 * @author hxm
 */
@Bean
public class TokenService {

    @Autowired
    private TokenDao tokenDao;


    public void addToken(String token, Integer userId) {
        tokenDao.removeToken(userId);
        TokenEntity tokenEntity = new TokenEntity();
        tokenEntity.setToken(token);
        tokenEntity.setUserId(userId);
        tokenEntity.setCreateTime(new Date());
        tokenDao.insert(tokenEntity);
    }

    public boolean removeToken(Integer userId) {
        return tokenDao.removeToken(userId);
    }

    public boolean exist(String token) {
        List<TokenEntity> select = tokenDao.selectList(new LambdaQueryWrapper<TokenEntity>().eq(TokenEntity::getToken, token));
        if (select.isEmpty()) {
            return false;
        }
        return true;
    }


}
