package com.system.service;

import cn.hserver.plugin.web.annotation.RequiresPermissions;
import cn.hserver.plugin.web.interfaces.PermissionAdapter;
import cn.hserver.plugin.web.router.RouterPermission;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.system.dao.ModuleDao;
import com.system.domain.entity.ModuleEntity;
import com.system.domain.vo.ModuleVO;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.ioc.annotation.Bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hxm
 */
@Bean
public class ModuleService {

    @Autowired
    private ModuleDao moduleDao;

    public void sync() {
        moduleDao.deleteAll();
        List<RouterPermission> routerPermissions = PermissionAdapter.getRouterPermissions();
        for (RouterPermission routerPermission : routerPermissions) {
            String name;
            if (routerPermission.getControllerName().trim().equals("")) {
                name = routerPermission.getControllerPackageName();
            } else {
                name = routerPermission.getControllerName();
            }
            RequiresPermissions requiresPermissions = routerPermission.getRequiresPermissions();
            String[] value = requiresPermissions.value();
            for (String s : value) {
                ModuleEntity moduleEntity = new ModuleEntity();
                moduleEntity.setName(requiresPermissions.name());
                moduleEntity.setControllerName(name);
                moduleEntity.setPermission(s);
                moduleDao.insert(moduleEntity);
            }
        }
    }

    public List<ModuleEntity> list() {
        return moduleDao.selectList(new LambdaQueryWrapper<ModuleEntity>().orderByAsc(ModuleEntity::getControllerName));
    }

    public List<ModuleVO> getModuleList() {
        List<ModuleEntity> select =moduleDao.selectList(new LambdaQueryWrapper<ModuleEntity>().orderByAsc(ModuleEntity::getControllerName));
        Map<String, ModuleVO> moduleVOS = new HashMap<>();
        for (ModuleEntity moduleEntity : select) {
            ModuleVO moduleVO = moduleVOS.get(moduleEntity.getControllerName());
            if (moduleVO == null) {
                moduleVO = new ModuleVO();
                moduleVO.setTitle(moduleEntity.getControllerName());
                moduleVO.setChildren(new ArrayList<>());
                moduleVOS.put(moduleEntity.getControllerName(), moduleVO);
            }
            ModuleVO moduleVO1 = new ModuleVO();
            moduleVO1.setPermission(moduleEntity.getPermission());
            if (moduleEntity.getName()!=null&&moduleEntity.getName().trim().length()>0){
                moduleVO1.setTitle(moduleEntity.getPermission()+"->"+moduleEntity.getName());
            }else {
                moduleVO1.setTitle(moduleEntity.getPermission());
            }
            moduleVO.getChildren().add(moduleVO1);
        }
        return new ArrayList(moduleVOS.values());
    }

}
