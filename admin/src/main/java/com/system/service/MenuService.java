package com.system.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.system.dao.MenuDao;
import com.system.dao.RolePermissionDao;
import com.system.domain.entity.MenuEntity;
import com.system.domain.vo.MenuTopVO;
import com.system.domain.vo.MenuVO;
import com.system.domain.vo.Meta;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.ioc.annotation.Bean;

import java.util.*;


/**
 * @author hxm
 */
@Bean
public class MenuService {

    @Autowired
    private MenuDao menuDao;

    @Autowired
    private RolePermissionDao rolePermissionDao;

    public void addMenu(MenuEntity menuEntity) {
        menuDao.insert(menuEntity);
    }

    public void updateMenu(MenuEntity menuEntity) {
        menuDao.updateById(menuEntity);
    }

    public void deleteMenu(Integer id) {
        menuDao.deleteById(id);
    }


    private List<MenuVO> getChild(Integer id, List<MenuEntity> rootMenu) {
        // 子菜单
        List<MenuVO> childList = new ArrayList<>();
        for (MenuEntity menuEntity : rootMenu) {
            // 遍历所有节点，将父菜单id与传过来的id比较
            if (menuEntity.getParentId() != null) {
                if (menuEntity.getParentId().equals(id)) {
                    MenuVO menuVO = new MenuVO();
                    menuVO.setId(menuEntity.getId());
                    menuVO.setParentId(menuEntity.getParentId());
                    menuVO.setPath(menuEntity.getPath());
                    menuVO.setComponent(menuEntity.getComponent());
                    menuVO.setName(menuEntity.getName());
                    menuVO.setTitle(menuEntity.getTitle());
                    menuVO.setIcon(menuEntity.getIcon());
                    menuVO.setSort(menuEntity.getSort());
                    menuVO.setRedirect(menuEntity.getRedirect());
                    menuVO.setMeta(new Meta(menuEntity.getTitle(), menuEntity.getIcon()));
                    childList.add(menuVO);
                }
            }
        }
        childList.sort(Comparator.comparingInt(MenuVO::getSort));
        for (MenuVO menu : childList) {
            menu.setChildren(getChild(menu.getId(), rootMenu));
        }
        // 递归退出条件
        if (childList.size() == 0) {
            return null;
        }
        return childList;
    }


    private List<MenuVO> getMenu(List<MenuEntity> menuEntities) {
        List<MenuVO> menuVOS = new ArrayList<>();
        for (MenuEntity menuEntity : menuEntities) {
            if (menuEntity.getParentId() == null) {
                MenuVO menuVO = new MenuVO();
                menuVO.setId(menuEntity.getId());
                menuVO.setParentId(menuEntity.getParentId());
                menuVO.setPath(menuEntity.getPath());
                menuVO.setComponent(menuEntity.getComponent());
                menuVO.setName(menuEntity.getName());
                menuVO.setTitle(menuEntity.getTitle());
                menuVO.setIcon(menuEntity.getIcon());
                menuVO.setSort(menuEntity.getSort());
                menuVO.setRedirect(menuEntity.getRedirect());
                menuVO.setMeta(new Meta(menuEntity.getTitle(), menuEntity.getIcon()));

                menuVOS.add(menuVO);
            }
        }
        menuVOS.sort(Comparator.comparingInt(MenuVO::getSort));
        // 为一级菜单设置子菜单，getChild是递归调用的
        for (MenuVO menuVO : menuVOS) {
            menuVO.setChildren(getChild(menuVO.getId(), menuEntities));
        }
        return menuVOS;
    }


    public List<MenuVO> list() {
        return getMenu(menuDao.selectList(new QueryWrapper<>()));
    }

    public List<MenuVO> menu(Integer[] roleIds) {
        List<MenuEntity> execute = menuDao.findMenuByRoleIds(roleIds);
        Set<Integer> integers = new HashSet<>();
        for (MenuEntity menuEntity : execute) {
            integers.add(menuEntity.getId());
            if (menuEntity.getParentId() != null) {
                integers.add(menuEntity.getParentId());
            }
        }
        List<MenuVO> list = list();

        List<MenuVO> tempIndex = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            //第一层
            MenuVO menuVO = list.get(i);
            if (!integers.contains(menuVO.getId())) {
                tempIndex.add(menuVO);
            }
        }
        list.removeAll(tempIndex);
        tempIndex.clear();
        for (int i = 0; i < list.size(); i++) {
            //第二层
            MenuVO menuVO = list.get(i);
            List<MenuVO> children = menuVO.getChildren();
            if (children != null) {
                for (int i1 = 0; i1 < children.size(); i1++) {
                    MenuVO menuVO1 = children.get(i1);
                    if (!integers.contains(menuVO1.getId())) {
                        tempIndex.add(menuVO1);
                    }
                }
                children.removeAll(tempIndex);
            }
        }
        tempIndex.clear();
        for (int i = 0; i < list.size(); i++) {
            //第二层
            MenuVO menuVO = list.get(i);
            List<MenuVO> children = menuVO.getChildren();
            if (children != null) {
                for (int i1 = 0; i1 < children.size(); i1++) {
                    MenuVO menuVO1 = children.get(i1);
                    List<MenuVO> children1 = menuVO1.getChildren();
                    if (children1 != null) {
                        for (int i2 = 0; i2 < children1.size(); i2++) {
                            MenuVO menuVO2 = children1.get(i2);
                            if (!integers.contains(menuVO2.getId())) {
                                tempIndex.add(menuVO2);
                            }
                        }
                        children1.removeAll(tempIndex);
                    }
                }
            }
        }
        tempIndex.clear();
        return list;
    }


    public List<MenuTopVO> listTop() {
        List<MenuEntity> menuEntities = menuDao.selectList(new LambdaQueryWrapper<MenuEntity>().isNull(MenuEntity::getParentId).orderByAsc(MenuEntity::getSort));
        List<MenuTopVO> menuVOS = new ArrayList<>();
        for (MenuEntity menuEntity : menuEntities) {
            MenuTopVO menuTopVO = new MenuTopVO();
            menuTopVO.setValue(menuEntity.getId());
            menuTopVO.setLabel(menuEntity.getTitle());
            List<MenuEntity> menuEntities1 = menuDao.selectList(new LambdaQueryWrapper<MenuEntity>().eq(MenuEntity::getParentId, menuEntity.getId()).orderByAsc(MenuEntity::getSort));
            for (MenuEntity entity : menuEntities1) {
                MenuTopVO menuVO1 = new MenuTopVO();
                menuVO1.setLabel(entity.getTitle());
                menuVO1.setValue(entity.getId());
                List<MenuTopVO> children = menuTopVO.getChildren();
                if (children == null) {
                    children = new ArrayList<>();
                    menuTopVO.setChildren(children);
                }
                children.add(menuVO1);
            }
            menuVOS.add(menuTopVO);
        }
        return menuVOS;
    }

}
