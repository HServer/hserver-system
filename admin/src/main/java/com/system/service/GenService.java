package com.system.service;

import com.system.dao.TableDao;
import com.system.domain.entity.TableEntity;
import com.system.domain.entity.TableInfoEntity;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.ioc.annotation.Bean;
import cn.hserver.core.ioc.annotation.Value;

import java.util.List;

@Bean
public class GenService {

    @Value("mysql.dbName")
    private String dbName;

    @Autowired
    private TableDao tableDao;

    public List<TableEntity> genEntityList() {
        return tableDao.getTables(dbName);
    }

    public List<TableInfoEntity> getTableInfo(String tableName) {
        return tableDao.getTableInfo(tableName);
    }

}
