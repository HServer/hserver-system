<template>
    <div style="padding: 1rem">
        <el-form :inline="true" :model="formInline" class="demo-form-inline">
            <#list tableList as table>
                <el-form-item label="${table.comment}">
                    <el-input v-model="formInline.${table.javaName}" placeholder="${table.comment}" />
                </el-form-item>
            </#list>

            <el-form-item>
                <el-button type="primary" @click="search">查询</el-button>
                <el-button type="success" @click="add">新增</el-button>
            </el-form-item>
        </el-form>
        <!--数据表-->
        <el-table
                v-loading="loading"
                :data="tableData"
                border
                style="width: 100%"
        >
            <#list tableList as table>
                <el-table-column
                        prop="${table.javaName}"
                        label="${table.comment}"
                />
            </#list>

            <el-table-column
                    label="操作"
            >
                <template slot-scope="scope">
                    <el-button type="text" size="small" @click="edit(scope.row)">编辑</el-button>
                    <el-button type="text" size="small" @click="del(scope.row)">删除</el-button>
                </template>
            </el-table-column>
        </el-table>
        <!--  分页-->
        <div class="block">
            <el-pagination
                    :current-page="currentPage"
                    :page-sizes="[2,10,50,100]"
                    :page-size="pageSize"
                    layout="total, sizes, prev, pager, next, jumper"
                    :total="total"
                    @size-change="handleSizeChange"
                    @current-change="handleCurrentChange"
            />
        </div>

        <el-dialog :visible.sync="dialogVisible" :title="dialogType==='edit'?'编辑':'添加'">
            <el-form :model="data" label-width="80px" label-position="left">
                <#list tableList as table>
                    <el-form-item label="${table.comment}">
                        <el-input v-model="data.${table.javaName}" placeholder="${table.comment}" />
                    </el-form-item>
                </#list>
            </el-form>
            <div style="text-align:right;">
                <el-button type="danger" @click="dialogVisible=false">取消</el-button>
                <el-button type="primary" @click="confirmRole">确定</el-button>
            </div>
        </el-dialog>
    </div>
</template>

<script>
    import { get${name}List, del${name}, upd${name}, add${name} } from '@/api/${nameLower}'
    const default${name} = {
    <#list tableList as table>
        ${table.javaName}: '',
    </#list>
    }
    export default {
        name: '${name}',
        data() {
            return {
                data: {},
                dialogType: '',
                dialogVisible: false,
                loading: true,
                formInline: {
                    id: ''
                },
                tableData: [],
                total: 0,
                pageSize: 10,
                currentPage: 1
            }
        },
        created() {
            this.get${name}List()
        },
        methods: {
            search() {
                const data = {
                    id: this.formInline.id,
                    page: this.currentPage,
                    pageSize: this.pageSize
                }
                get${name}List(data).then(res => {
                    if (res.code === 200) {
                        this.tableData = res.data.records
                        this.total = res.data.total
                    }
                })
                this.loading = false
            },
            // 获取数据表
            async get${name}List() {
                const data = {
                    id: this.formInline.id,
                    page: this.currentPage,
                    pageSize: this.pageSize
                }
                await get${name}List(data).then(res => {
                    if (res.code === 200) {
                        this.tableData = res.data.records
                        this.total = res.data.total
                    }
                })
                this.loading = false
            },
            del(row) {
                this.$confirm('此操作将永久删除该数据, 是否继续?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(() => {
                    del${name}({ id: row.id }).then(res => {
                        if (res.code === 200) {
                            this.$message({
                                type: 'success',
                                message: '删除成功!'
                            })
                            this.get${name}List()
                        }
                    })
                }).catch(() => {
                    this.$message({
                        type: 'info',
                        message: '已取消删除'
                    })
                })
            },
            edit(row) {
                this.data = row
                this.dialogType = 'edit'
                this.dialogVisible = true
            },
            add() {
                this.data = Object.assign({}, default${name})
                this.dialogType = 'new'
                this.dialogVisible = true
            },
            async confirmRole() {
                const isEdit = this.dialogType === 'edit'
                if (isEdit) {
                    await upd${name}(this.data)
                    this.$notify({
                        title: 'Success',
                        dangerouslyUseHTMLString: true,
                        message: `编辑成功`,
                        type: 'success'
                    })
                } else {
                    await add${name}(this.data)
                    this.$notify({
                        title: 'Success',
                        dangerouslyUseHTMLString: true,
                        message: `添加成功`,
                        type: 'success'
                    })
                }
                this.get${name}List()
                this.dialogVisible = false
            },
            // 分页方法
            // 点击下一页
            handleCurrentChange(val) {
                this.currentPage = val
                this.search()
            },
            handleSizeChange(val) {
                this.pageSize = val
                this.search()
            }
        }
    }
</script>

<style scoped>

</style>
