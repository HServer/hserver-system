package com.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("${tableName}")
public class ${name}Entity {
<#list tableList as table>
    <#if table.comment!="">
    /**
     * ${table.comment}
     */
    </#if>
    <#if table.hasPri>
    @TableId(type = IdType.AUTO)
    </#if>
    private ${table.javaType} ${table.javaName};

</#list>
}
