package com.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.hserver.core.ioc.annotation.Autowired;
import cn.hserver.core.ioc.annotation.Bean;

import java.util.Date;
import java.util.Map;

@Bean
public class ${name}Service {

    @Autowired
    private ${name}Dao ${nameLower}Dao;

    public IPage<${name}Entity> get${name}List(Map<String, Object> map) {
        Page<${name}Entity> pages = new Page<>(Integer.parseInt(map.get("page").toString()), Integer.parseInt(map.get("pageSize").toString()));
        return ${nameLower}Dao.listPage(pages,map);
    }

    public boolean del${name}(Integer id) {
        return ${nameLower}Dao.deleteById(id) > 0;
    }

    public boolean add${name}(${name}Entity data) {
        data.setCreateTime(new Date());
        return ${nameLower}Dao.insert(data) > 0;
    }

    public boolean up${name}(${name}Entity data) {
        return ${nameLower}Dao.updateById(data) > 0;
    }
}