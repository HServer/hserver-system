import request from '@/utils/request'

export function get${name}List(params) {
    return request({
    url: '/system/${nameLower}/get${name}List',
    method: 'get',
    params
    })
}

export function del${name}(params) {
    return request({
    url: '/system/${nameLower}/del${name}',
    method: 'get',
    params
    })
}

export function add${name}(data) {
    return request({
    url: '/system/${nameLower}/add${name}',
    method: 'post',
    data
    })
}

export function up${name}(data) {
    return request({
    url: '/system/${nameLower}/up${name}',
    method: 'post',
    data
    })
}
