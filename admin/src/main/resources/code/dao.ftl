package com.system.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

@Mybatis
public interface ${name}Dao extends BaseMapper<${name}Entity> {
    IPage<${name}Entity> listPage(Page<?> page, @Param("data") Map<String, Object> data);
}