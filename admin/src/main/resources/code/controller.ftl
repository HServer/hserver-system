package com.system.controller;

import cn.hserver.plugin.web.annotation.Controller;
import cn.hserver.plugin.web.annotation.GET;
import cn.hserver.plugin.web.annotation.POST;
import cn.hserver.plugin.web.annotation.RequiresPermissions;
import cn.hserver.core.ioc.annotation.*;
import cn.hserver.core.server.util.JsonResult;

import java.util.Map;

@Controller(value = "/system/${nameLower}/", name = "${name}管理")
public class ${name}Controller {

    @Autowired
    private ${name}Service ${nameLower}Service;

    @RequiresPermissions(value = "/system/${nameLower}/get${name}List", name = "列表")
    @GET("get${name}List")
    public JsonResult get${name}List(Map<String, Object> map) {
        return JsonResult.ok().put("data", ${nameLower}Service.getLogList(map));
    }

    @RequiresPermissions(value = "/system/${nameLower}/del${name}", name = "删除")
    @GET("del${name}")
    public JsonResult del${name}(Integer id) {
        return ${nameLower}Service.del${name}(id) ? JsonResult.ok() : JsonResult.error();
    }

    @RequiresPermissions(value = "/system/${nameLower}/add${name}", name = "新增")
    @POST("add${name}")
    public JsonResult add${name}(${name}Entity ${nameLower}Entity) {
        return ${nameLower}Service.add${name}(${nameLower}Entity) ? JsonResult.ok() : JsonResult.error();
    }

    @RequiresPermissions(value = "/system/${nameLower}/up${name}", name = "修改")
    @POST("/up${name}")
    public JsonResult up${name}(${name}Entity ${nameLower}Entity) {
        return ${nameLower}Service.up${name}(${nameLower}Entity) ? JsonResult.ok() : JsonResult.error();
    }
}