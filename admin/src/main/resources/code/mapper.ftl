<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.system.dao.${name}Dao">
    <select id="listPage" resultType="com.system.domain.entity.${name}Entity">
        SELECT * from ${tableName}  where 1=1
        <#list tableList as table>
        <if test="data.${table.javaName} != null and data.${table.javaName} != ''">
            AND ${table.name} = <#noparse>#{data.</#noparse>${table.javaName}}
        </if>
        </#list>
        order by id desc
    </select>
</mapper>