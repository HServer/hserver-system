/*
 Navicat Premium Data Transfer

 Source Server         : 路由MySQL
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : 192.168.5.1:3306
 Source Schema         : hserver_db

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 20/04/2022 22:14:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `redirect` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `parent_id` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (19, '/api', 'Layout', '', 'api文档', 'api文档', 'el-icon-s-platform', NULL, 3);
INSERT INTO `sys_menu` VALUES (20, '/api/index', 'api/index', '', '系统文档', '系统文档', 'el-icon-s-order', 19, 1);
INSERT INTO `sys_menu` VALUES (18, 'http://baidu.com', 'Layout', '', '百度测试', '百度测试', 'el-icon-s-promotion', NULL, 4);
INSERT INTO `sys_menu` VALUES (13, '/system', 'Layout', '', '系统管理', '系统管理', 'el-icon-s-platform', NULL, 1);
INSERT INTO `sys_menu` VALUES (14, '/system/user', 'system/user', '', '用户管理', '用户管理', 'el-icon-user-solid', 13, 1);
INSERT INTO `sys_menu` VALUES (15, '/system/role', 'system/role', '', '角色管理', '角色管理', 'peoples', 13, 2);
INSERT INTO `sys_menu` VALUES (16, '/system/menu', 'system/menu', '', '菜单管理', '菜单管理', 'tree-table', 13, 3);
INSERT INTO `sys_menu` VALUES (17, '/system/module', 'system/module', '', '模块管理', '模块管理', 'el-icon-s-management', 13, 4);
INSERT INTO `sys_menu` VALUES (28, '/api/test', 'api/test', '', '测试', '测试', '', 19, 0);
INSERT INTO `sys_menu` VALUES (31, '/api/test/test', 'api/test/index', '', '测试', '测试', '', 28, 0);
INSERT INTO `sys_menu` VALUES (30, '/api/test/index', 'api/test/index', '', '测试2', '测试2', '', 28, 0);
INSERT INTO `sys_menu` VALUES (32, '/tool', 'Layout', '', '系统工具', '系统工具', 'el-icon-s-platform', NULL, 2);
INSERT INTO `sys_menu` VALUES (33, '/tool/gen', 'tool/gen', '', '代码生成', '代码生成', 'el-icon-s-platform', 32, 0);

-- ----------------------------
-- Table structure for sys_module
-- ----------------------------
DROP TABLE IF EXISTS `sys_module`;
CREATE TABLE `sys_module`  (
  `permission` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `controller_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`permission`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '模块权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_module
-- ----------------------------
INSERT INTO `sys_module` VALUES ('post:/system/menu/updateMenu', '更新菜单', '菜单管理');
INSERT INTO `sys_module` VALUES ('post:/system/role/add', '添加角色', '角色管理');
INSERT INTO `sys_module` VALUES ('post:/system/menu/addMenu', '添加菜单', '菜单管理');
INSERT INTO `sys_module` VALUES ('post:/system/user/multiDelete', '删除多个用户', '用户管理');
INSERT INTO `sys_module` VALUES ('post:/system/user/add', '添加用户', '用户管理');
INSERT INTO `sys_module` VALUES ('post:/system/role/updateMenuPermission', '更新菜单权限', '角色管理');
INSERT INTO `sys_module` VALUES ('post:/system/role/updateRole', '更新角色', '角色管理');
INSERT INTO `sys_module` VALUES ('post:/system/user/logout', '登出', '用户管理');
INSERT INTO `sys_module` VALUES ('post:/system/user/edit', '用户列表', '用户管理');
INSERT INTO `sys_module` VALUES ('post:/system/role/updateModulePermission', '更新模块权限', '角色管理');
INSERT INTO `sys_module` VALUES ('get:/system/role/getModulePermission', '获取模块权限', '角色管理');
INSERT INTO `sys_module` VALUES ('get:/system/role/list', '角色列表', '角色管理');
INSERT INTO `sys_module` VALUES ('get:/system/user/delete', '删除用户', '用户管理');
INSERT INTO `sys_module` VALUES ('get:/system/role/deleteRole', '删除角色', '角色管理');
INSERT INTO `sys_module` VALUES ('get:/system/menu/deleteMenu', '删除菜单', '菜单管理');
INSERT INTO `sys_module` VALUES ('get:/system/module/sync', '同步权限', '模块管理');
INSERT INTO `sys_module` VALUES ('get:/system/menu/listTop', '获取一二级的菜单', '菜单管理');
INSERT INTO `sys_module` VALUES ('get:/system/role/getMenuPermission', '获取菜单权限', '角色管理');
INSERT INTO `sys_module` VALUES ('get:/system/module/getModuleList', '获取模块列表', '模块管理');
INSERT INTO `sys_module` VALUES ('get:/system/menu/menu', '获取当前用户的列表', '菜单管理');
INSERT INTO `sys_module` VALUES ('get:/system/user/info', '获取用户信息', '用户管理');
INSERT INTO `sys_module` VALUES ('get:/system/menu/list', '获取全部菜单列表', '菜单管理');
INSERT INTO `sys_module` VALUES ('get:/system/module/list', '所有权限列表', '模块管理');
INSERT INTO `sys_module` VALUES ('get:/system/user/list', '用户列表', '用户管理');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通用户', '用户');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `type` bit(1) NULL DEFAULT NULL,
  `role_id` int(0) NULL DEFAULT NULL,
  `m_id` int(0) NULL DEFAULT NULL COMMENT '菜单ID',
  `permission` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 246 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (45, b'0', 11, 4, NULL);
INSERT INTO `sys_role_permission` VALUES (46, b'1', 11, NULL, 'get:/system/module/list');
INSERT INTO `sys_role_permission` VALUES (47, b'1', 11, NULL, 'get:/system/module/sync');
INSERT INTO `sys_role_permission` VALUES (199, b'0', 2, 27, NULL);
INSERT INTO `sys_role_permission` VALUES (200, b'0', 2, 20, NULL);
INSERT INTO `sys_role_permission` VALUES (222, b'1', 1, NULL, 'get:/system/user/list');
INSERT INTO `sys_role_permission` VALUES (223, b'1', 1, NULL, 'get:/system/user/info');
INSERT INTO `sys_role_permission` VALUES (224, b'1', 1, NULL, 'get:/system/user/delete');
INSERT INTO `sys_role_permission` VALUES (225, b'1', 1, NULL, 'post:/system/user/edit');
INSERT INTO `sys_role_permission` VALUES (226, b'1', 1, NULL, 'post:/system/user/logout');
INSERT INTO `sys_role_permission` VALUES (227, b'1', 1, NULL, 'post:/system/user/add');
INSERT INTO `sys_role_permission` VALUES (228, b'1', 1, NULL, 'post:/system/user/multiDelete');
INSERT INTO `sys_role_permission` VALUES (229, b'1', 1, NULL, 'get:/system/menu/menu');
INSERT INTO `sys_role_permission` VALUES (230, b'1', 1, NULL, 'get:/system/menu/list');
INSERT INTO `sys_role_permission` VALUES (231, b'1', 1, NULL, 'get:/system/menu/listTop');
INSERT INTO `sys_role_permission` VALUES (232, b'1', 1, NULL, 'get:/system/menu/deleteMenu');
INSERT INTO `sys_role_permission` VALUES (233, b'1', 1, NULL, 'post:/system/menu/addMenu');
INSERT INTO `sys_role_permission` VALUES (234, b'1', 1, NULL, 'post:/system/menu/updateMenu');
INSERT INTO `sys_role_permission` VALUES (235, b'1', 1, NULL, 'get:/system/role/getMenuPermission');
INSERT INTO `sys_role_permission` VALUES (236, b'1', 1, NULL, 'get:/system/role/deleteRole');
INSERT INTO `sys_role_permission` VALUES (237, b'1', 1, NULL, 'get:/system/role/list');
INSERT INTO `sys_role_permission` VALUES (238, b'1', 1, NULL, 'get:/system/role/getModulePermission');
INSERT INTO `sys_role_permission` VALUES (239, b'1', 1, NULL, 'post:/system/role/updateModulePermission');
INSERT INTO `sys_role_permission` VALUES (240, b'1', 1, NULL, 'post:/system/role/updateRole');
INSERT INTO `sys_role_permission` VALUES (241, b'1', 1, NULL, 'post:/system/role/updateMenuPermission');
INSERT INTO `sys_role_permission` VALUES (242, b'1', 1, NULL, 'post:/system/role/add');
INSERT INTO `sys_role_permission` VALUES (243, b'1', 1, NULL, 'get:/system/module/list');
INSERT INTO `sys_role_permission` VALUES (244, b'1', 1, NULL, 'get:/system/module/getModuleList');
INSERT INTO `sys_role_permission` VALUES (245, b'1', 1, NULL, 'get:/system/module/sync');
INSERT INTO `sys_role_permission` VALUES (246, b'1', 2, NULL, 'get:/system/user/list');
INSERT INTO `sys_role_permission` VALUES (247, b'1', 2, NULL, 'post:/system/user/multiDelete');
INSERT INTO `sys_role_permission` VALUES (248, b'1', 2, NULL, 'get:/system/user/info');
INSERT INTO `sys_role_permission` VALUES (249, b'1', 2, NULL, 'post:/system/user/add');
INSERT INTO `sys_role_permission` VALUES (250, b'1', 2, NULL, 'post:/system/user/logout');
INSERT INTO `sys_role_permission` VALUES (251, b'1', 2, NULL, 'get:/system/user/delete');
INSERT INTO `sys_role_permission` VALUES (252, b'1', 2, NULL, 'post:/system/user/edit');
INSERT INTO `sys_role_permission` VALUES (253, b'1', 2, NULL, 'post:/system/menu/updateMenu');
INSERT INTO `sys_role_permission` VALUES (254, b'1', 2, NULL, 'post:/system/menu/addMenu');
INSERT INTO `sys_role_permission` VALUES (255, b'1', 2, NULL, 'get:/system/menu/deleteMenu');
INSERT INTO `sys_role_permission` VALUES (256, b'1', 2, NULL, 'get:/system/menu/listTop');
INSERT INTO `sys_role_permission` VALUES (257, b'1', 2, NULL, 'get:/system/menu/list');
INSERT INTO `sys_role_permission` VALUES (258, b'1', 2, NULL, 'get:/system/menu/menu');
INSERT INTO `sys_role_permission` VALUES (259, b'1', 2, NULL, 'get:/system/role/list');
INSERT INTO `sys_role_permission` VALUES (260, b'1', 2, NULL, 'get:/system/role/getModulePermission');
INSERT INTO `sys_role_permission` VALUES (261, b'1', 2, NULL, 'post:/system/role/updateModulePermission');
INSERT INTO `sys_role_permission` VALUES (262, b'1', 2, NULL, 'get:/system/role/deleteRole');
INSERT INTO `sys_role_permission` VALUES (263, b'1', 2, NULL, 'post:/system/role/updateRole');
INSERT INTO `sys_role_permission` VALUES (264, b'1', 2, NULL, 'post:/system/role/updateMenuPermission');
INSERT INTO `sys_role_permission` VALUES (265, b'1', 2, NULL, 'get:/system/role/getMenuPermission');
INSERT INTO `sys_role_permission` VALUES (266, b'1', 2, NULL, 'post:/system/role/add');
INSERT INTO `sys_role_permission` VALUES (267, b'1', 2, NULL, 'get:/system/module/list');
INSERT INTO `sys_role_permission` VALUES (268, b'1', 2, NULL, 'get:/system/module/getModuleList');
INSERT INTO `sys_role_permission` VALUES (269, b'1', 2, NULL, 'get:/system/module/sync');
INSERT INTO `sys_role_permission` VALUES (318, b'0', 1, 32, NULL);
INSERT INTO `sys_role_permission` VALUES (319, b'0', 1, 33, NULL);
INSERT INTO `sys_role_permission` VALUES (320, b'0', 1, 13, NULL);
INSERT INTO `sys_role_permission` VALUES (321, b'0', 1, 14, NULL);
INSERT INTO `sys_role_permission` VALUES (322, b'0', 1, 15, NULL);
INSERT INTO `sys_role_permission` VALUES (323, b'0', 1, 16, NULL);
INSERT INTO `sys_role_permission` VALUES (324, b'0', 1, 17, NULL);
INSERT INTO `sys_role_permission` VALUES (325, b'0', 1, 19, NULL);
INSERT INTO `sys_role_permission` VALUES (326, b'0', 1, 28, NULL);
INSERT INTO `sys_role_permission` VALUES (327, b'0', 1, 31, NULL);
INSERT INTO `sys_role_permission` VALUES (328, b'0', 1, 30, NULL);
INSERT INTO `sys_role_permission` VALUES (329, b'0', 1, 20, NULL);
INSERT INTO `sys_role_permission` VALUES (330, b'0', 1, 18, NULL);

-- ----------------------------
-- Table structure for sys_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_token`;
CREATE TABLE `sys_token`  (
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_id` int(0) NULL DEFAULT NULL,
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'token' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_token
-- ----------------------------
INSERT INTO `sys_token` VALUES ('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlSWRzIjpbMSwyXSwidXNlcklkIjoxLCJ1c2VybmFtZSI6ImFkbWluIn0.6NOcL0lPys2bfWhX0giLBr6KigkXquAYH1WToT_od34', 1, '2022-04-20 13:47:40');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `state` int(0) NULL DEFAULT 0 COMMENT '状态',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', 's05bse6q2qlb9qblls96s592y55y556s', '黑小马', 'https://portrait.gitee.com/uploads/avatars/user/631/1893827_heixiaomas_admin_1578961913.png!avatar100', 0, NULL, '2020-09-29 09:41:18');
INSERT INTO `sys_user` VALUES (2, 'heixiaoma', 's05bse6q2qlb9qblls96s592y55y556s', 'heixiaoma', 'https://portrait.gitee.com/uploads/avatars/user/631/1893827_heixiaomas_admin_1578961913.png!avatar100', 0, NULL, '2020-09-30 08:39:58');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_id` int(0) NULL DEFAULT NULL,
  `role_id` int(0) NULL DEFAULT NULL,
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (3, NULL, 1, NULL);
INSERT INTO `sys_user_role` VALUES (4, NULL, 1, NULL);
INSERT INTO `sys_user_role` VALUES (5, NULL, 1, NULL);
INSERT INTO `sys_user_role` VALUES (6, NULL, 1, NULL);
INSERT INTO `sys_user_role` VALUES (39, NULL, 1, NULL);
INSERT INTO `sys_user_role` VALUES (40, NULL, 2, NULL);
INSERT INTO `sys_user_role` VALUES (51, 1, 1, NULL);
INSERT INTO `sys_user_role` VALUES (52, 1, 2, NULL);
INSERT INTO `sys_user_role` VALUES (54, 2, 2, NULL);

SET FOREIGN_KEY_CHECKS = 1;
