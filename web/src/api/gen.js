import request from '@/utils/request'

export function getList() {
  return request({
    url: '/system/gen/list',
    method: 'get'
  })
}

export function gen(params) {
  return request({
    url: '/system/gen/gen',
    method: 'get',
    params
  })
}
