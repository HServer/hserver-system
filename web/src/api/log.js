import request from '@/utils/request'

export function getLogList(params) {
  return request({
    url: '/system/log/getLogList',
    method: 'get',
    params
  })
}

export function delLog(params) {
  return request({
    url: '/system/log/delLog',
    method: 'get',
    params
  })
}
export function addLog(data) {
  return request({
    url: '/system/log/addLog',
    method: 'post',
    data
  })
}
export function updLog(data) {
  return request({
    url: '/system/log/updLog',
    method: 'post',
    data
  })
}
